import serial
import time
import sys
from threading import Thread
from helper import *
from queue import Queue

print("Welcome to the Visible Light Communications chat!")
addr = input("Please enter your address : ")
dest = input("Please enter the destination address : ")
port = input("Please enter your serial device port: ")
print("Setting up...")

s = setup(port, addr)

toSend = Queue(maxsize=100)
toRead = Queue(maxsize=100)

chatThread = Thread(target=talkWith, kwargs={'s':s, 'dest':dest, 'toSend':toSend, 'toRead':toRead})
chatThread.start()

print("Type to start chatting!")
while True:
    try:
        send = input()
        if len(send) < 200:
            enqueueMsg(toSend, send, dest)
        else:
            print(">Error :Message too long")
    except KeyboardInterrupt:
        sys.exit() #on ctrl-c terminate program

