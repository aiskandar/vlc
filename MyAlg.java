package layer2_802Algorithms;

import layer1_802Phy.JE802Phy;
import layer1_802Phy.JE802PhyMode;
import layer2_80211Mac.JE802_11BackoffEntity;
import layer2_80211Mac.JE802_11Mac;
import layer2_80211Mac.JE802_11MacAlgorithm;
import plot.JEMultiPlotter;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class MyAlg extends JE802_11MacAlgorithm {

	static class PID {

		double current_error, prev_error, integral = 0;

		final double target;
		final double kp;
		final double ki;
		final double kd;

		public double output;


		PID(double target, double kp, double ki, double kd) {
			this.target = target;
			this.kp = kp;
			this.ki = ki;
			this.kd = kd;
		}

		public double computePID(double input, double time_step){
			current_error = target - input;
			double p = current_error;
			integral += current_error * time_step;
			double i = integral;
			double d = (current_error - prev_error)/time_step;
			//System.out.printf("CURR: %f, p: %f , i: %f , d: %f %n", current_error, p, i, d);
			prev_error = current_error;
			output = kp * p + ki * i + kd * d;
			return output;
		}

	}

	private JE802_11BackoffEntity theBackoffEntityAC01;

	private double theSamplingTime_sec;
	private PID pid = new PID(100, 0.02, 0.01, 0.06);
	public MyAlg(String name, JE802_11Mac mac) {
		super(name, mac);
		this.theBackoffEntityAC01 = this.mac.getBackoffEntity(1);
		message("This is station " + this.dot11MACAddress.toString() +". MobComp algorithm: '" + this.algorithmName + "'.", 100);
	}

	int prev_col = 0;
	long prev_ack = 0;

	@SuppressWarnings("unused")
	@Override
	public void compute() {

		this.mac.getMlme().setTheIterationPeriod(0.1);
		this.theSamplingTime_sec = this.mac.getMlme().getTheIterationPeriod().getTimeS();

		this.theSamplingTime_sec = this.theSamplingTime_sec + 0;


		int aQueueSize = this.theBackoffEntityAC01.getQueueSize();
		int aCollisionCnt = this.theBackoffEntityAC01.getTheCollisionCnt();
		long aAckCnt = this.theBackoffEntityAC01.getTheAckCnt();
		JE802PhyMode aCurrentPhyMode = this.mac.getPhy().getCurrentPhyMode();
		this.mac.getPhy().setCurrentPhyMode("64QAM34");
		int collDIFF = (aCollisionCnt - prev_col);
		long ackDIFF = (aAckCnt - prev_ack);
		double collAckDIFF = ((double) collDIFF / ackDIFF);
		double output = pid.computePID(ackDIFF, theSamplingTime_sec);

		prev_col = aCollisionCnt;
		prev_ack = aAckCnt;

		int currentMin = this.theBackoffEntityAC01.getDot11EDCACWmin();
		int currentAIFSN = this.theBackoffEntityAC01.getDot11EDCAAIFSN();

		double newMin = currentMin;
		double newAIFSN = currentAIFSN;
		
		if (output > 10){
			newMin *= 0.5;
			newAIFSN *= 0.5;
		} else if (output < -10) {
			newMin *= 1.5;
			newAIFSN *= 1.5;
		} else {
			if (currentMin > 15){
				newMin *= 0.8;
			} else {
				newMin *= 1.5;
			}
			if (currentAIFSN > 3){
				newAIFSN *= 0.85;
			} else {
				newAIFSN *= 1.5;
			}
		}

		this.theBackoffEntityAC01.setDot11EDCAAIFSN(Math.max(1, Math.min(6, (int) Math.round(newAIFSN))));
		this.theBackoffEntityAC01.setDot11EDCACWmin(Math.max(2, Math.min(25, (int) Math.round(newMin))));

	}

	@Override
	public void plot() {
		if (plotter == null) {
			plotter = new JEMultiPlotter("PID Controller, Station " + this.dot11MACAddress.toString(), "max", "time [s]", "MAC Queue", this.theUniqueEventScheduler.getEmulationEnd().getTimeMs() / 1000.0, true);
			plotter.addSeries("current");
			plotter.display();
		}
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue() / 1000.0, theBackoffEntityAC01.getTheCollisionCnt(), 1);
		plotter.plot(((Double) theUniqueEventScheduler.now().getTimeMs()).doubleValue() / 1000.0, pid.output, 0);
	}

}
