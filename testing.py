import serial
import time
import sys
from threading import Thread
from helper import *
from queue import Queue

def testSetup(port : str, address : str):

    s = serial.Serial(port= port,baudrate= 115200,timeout=1) #opens a serial port (resets the device!)
    time.sleep(2) #give the device some time to startup (2 seconds)

    #write to the device’s serial port
    s.write(str.encode(f"a[{address}]\n")) #set the device address to AB
    time.sleep(0.1) #wait for settings to be applied
    s.write(str.encode("c[1,0,5]\n")) #set number of retransmissions to 5
    time.sleep(0.1) #wait for settings to be applied
    s.write(str.encode("c[0,1,30]\n")) #set FEC threshold to 30 (apply FEC to packets with payload >= 30)
    time.sleep(0.1)
    s.write(str.encode("c[0,2,5]\n")) 
    time.sleep(0.1)
    return s

print("Welcome to the Visible Light Communications chat!")
type = input("Enter S for Sending and R for Receiving  : ")
addr = input("Please enter your address : ")
dest = input("Please enter the destination address : ")
port = input("Please enter your serial device port: ")
print("Setting up...")
s = testSetup(port, addr)


delays = []
payload_sizes = [1, 100, 180]

distance = 1
fec = 30
busy = 5

if type == "S":
    while payload_sizes:
        delays = []
        payload_size = payload_sizes.pop(0)
        print(f"Sending Payload Size {payload_size}")
        fName = f"{distance}_{payload_size}_{fec}_{busy}"
        f = open(f"stats/{fName}.csv", "w")
        for _ in range(20):
            (msg, addr) = ("A" * int(payload_size), dest)
            start = time.time()
            s.write(str.encode(f"m[{msg}\0,{addr}]\n", "ascii"))
            msg = bytes.decode(s.read_until(), "ascii").strip()
            while not msg.startswith("m[D]"):
                msg = bytes.decode(s.read_until(), "ascii").strip()
            end = time.time()
            delays.append(1000 * (end - start))
            time.sleep(0.01)
        print(f"Avg Delay for Payload Size of {payload_size}: ", sum(delays) / len(delays))
        print(f"Thrp for Payload Size of {payload_size}: ", (1000 * payload_size * 20 * 8 / sum(delays)))
        for i in delays:
            f.write(str(i) + '\n')
        f.close()
    
elif type == "R":
    while True:
        try:
            msg = bytes.decode(s.read_until(), "ascii").strip()
            if msg.startswith("m[R,D"):
                print(msg)
        except serial.SerialException:
            continue

    

