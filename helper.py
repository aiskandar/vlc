import serial
import time
import sys
from queue import *

def setup(port : str, address : str):

    s = serial.Serial(port= port,baudrate= 115200,timeout=1) #opens a serial port (resets the device!)
    time.sleep(2) #give the device some time to startup (2 seconds)

    #write to the device’s serial port
    s.write(str.encode(f"a[{address}]\n")) #set the device address to AB
    time.sleep(0.1) #wait for settings to be applied
    s.write(str.encode("c[1,0,5]\n")) #set number of retransmissions to 5
    time.sleep(0.1) #wait for settings to be applied
    s.write(str.encode("c[0,1,30]\n")) #set FEC threshold to 30 (apply FEC to packets with payload >= 30)
    time.sleep(0.1) #wait for settings to be applied
    #
    return s

def talkWith(s : serial, dest : str, toSend : Queue, toRead : Queue):

    #read from the device’s serial port (should be done in a separate thread)
    while True: #while not terminated
        try:
            dequeueMsg(toRead)   
            readMsg(s,toRead, dest)
            sendMsg(s,toSend, dest)
        except serial.SerialException:
            continue
        except KeyboardInterrupt:
            sys.exit() #on ctrl-c terminate program 

def enqueueMsg(q : Queue, msg : str, dest : str):
    q.put((msg,dest), timeout=0.5)
  
def dequeueMsg(q : Queue):
    try:
        msg = q.get(timeout=0.5)
        q.task_done()
    except Empty:
        return 
    (msg,sender) = msg
    print(f"{sender} : {msg}")
    return

def readMsg(s : serial, q : Queue, dest: str):
    msg = bytes.decode(s.read_until(), "ascii").strip()
    if msg.startswith("m[R,D"):
        q.put((msg[6:-1], dest), timeout=0.5)

def sendMsg(s : serial, q : Queue, dest: str):
    try:
        (msg, addr) = q.get(timeout=0.5)
        q.task_done()
    except Empty:
        return
    s.write(str.encode(f"m[{msg}\0,{addr}]\n", "ascii")) #send message to device
    msg = bytes.decode(s.read_until(), "ascii").strip()
    while not msg.startswith("m[D]"):
        msg = bytes.decode(s.read_until(), "ascii").strip()

